from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import pathlib
import time
import shutil
import glob

chrome_options = Options()
chrome_options.add_experimental_option('prefs', {
    "plugins.plugins_list": [{"enabled":False,"name":"Chrome PDF Viewer"}],
    "download": {
    "prompt_for_download": False,
    "default_directory"  : './'
    }
})

class WordPress:
    def __init__(self):
        firefoxProfile = webdriver.FirefoxProfile()
        firefoxProfile.set_preference("dom.webnotifications.enabled", False)
        self.driver = webdriver.Chrome(
            executable_path=r"./chromedriver"
        )

    def save_in_text(name_file, for_save):
        with open(name_file, 'w') as file:
            file.writelines(for_save.text)

    def send_key_to_textarea(name_file, input_area):
        with open(name_file, 'r') as file:
            for text in file:
                input_area.send_keys(text)

    def click_in_buttons(driver):
        spin_button = driver.find_elements_by_xpath("//a[@class='btn btn-primary']")
        time.sleep(5)
        spin_button[0].click()
        time.sleep(5)
        finish_button = driver.find_elements_by_xpath("//a[@class='btn btn-primary']")
        time.sleep(5)
        finish_button[1].click()
        time.sleep(5)
        download_button = driver.find_elements_by_xpath("//input[@value='Download']")
        time.sleep(5)
        download_button[0].click()

    def search_page(self):
        driver = self.driver
        driver.get("https://www.google.com")
        time.sleep(3)
        input_google = driver.find_element_by_xpath("//input[@name='q']")
        input_google.clear()
        input_google.send_keys("“Keyword1” site:wordpress.com")
        input_google.submit()
        time.sleep(3)
        result_of_search = driver.find_element_by_xpath("//h3[@class='LC20lb DKV0Md']").click()
        #print(driver.page_source)
        time.sleep(5)
        result_page = driver.find_element_by_id('content')
        time.sleep(5)        

        WordPress.save_in_text('text.txt', result_page)

        driver.get('https://seotoolscentre.com/paraphrase-tool')
        input_spin = driver.find_element_by_xpath("//textarea[@id='data']")
        time.sleep(5)

        WordPress.send_key_to_textarea('text.txt', input_spin)
        time.sleep(5)
        WordPress.click_in_buttons(self.driver)




test = WordPress()
test.search_page()